/*全能微站出品
微站申请地址：https://www.44api.com/weizhan.html
188元，不用备案，不需懂后台！
学前端编程，打造私域流量的利器

*/
var wzid=717;
var localStorage={
    clear:function(){return uni.clearStorageSync();},
    removeItem:function(key){uni.removeStorage({key: key});},
    setItem:function(key,data){
        uni.setStorage({key: key,data: data})
    }
}/*本地缓存优化*/
var imgcdn='https://hkbig.44api.com/upload/';/*大图域名*/
var imgswt='https://hki.44api.com/upload/';/*缩微图域名*/
var timestamp=new Date().getTime();/*当前时间 */
/*字符是否在数组中，模式2：查找；模式1：查找并删除再加入到尾部 */
var str_in_array=function(search, array,mode) {
 mode = typeof mode !== 'undefined' ? mode : 2;
 if(mode==2 && array==false) return false;
 for (var i in array) {
  if (array[i] == search) {
   if(mode==2) return true;
  array.splice(i,1);
  break;
  }
 }
 if(mode==1) array.push(search);
 if(mode==2) return false;
 return array
}
/*数字格式化如：01 */
function Appendzero(obj){
if(obj<10) return "0" +""+ obj;
else return obj;
}
/*时间数值 */
var datetime = Date.parse(new Date())/1000;
/*时间格式化*/
function timeStamp2String(time,mode){
mode = typeof mode !== 'undefined' ? mode : false;
if(mode==false){
let datetime = new Date()
datetime.setTime(time * 1000);
return datetime.getFullYear() + "-" + Appendzero(datetime.getMonth() + 1) + "-" + Appendzero(datetime.getDate())+' '+ Appendzero(datetime.getHours())+':'+ Appendzero(datetime.getMinutes())+':'+ Appendzero(datetime.getSeconds());
}
let limit=datetime-time;
if(limit<60){
return "刚刚";
}else if(limit>=60 && limit<3600){
return Math.floor(limit/60)+"分钟前";
}else if(limit>=3600 && limit<86400){
return Math.floor(limit/3600)+"小时前";
}else if(limit>=86400 && limit<2592000){
return Math.floor(limit/86400)+"天前";
}else if(limit>=2592000 && limit<31104000){
return Math.floor(limit/2592000)+"个月前";
}else{
    let datetime = new Date()
return datetime.getFullYear() + "-" + Appendzero(datetime.getMonth() + 1) + "-" + Appendzero(datetime.getDate())
 }
}
/*是否是数字*/
function isNumber(value) {
  var patrn = /^[0-9]*$/;
  if (patrn.exec(value) == null || value == "") {
    return false;
  } else {
    return true;
  }
}
/*数组比较*/
var in_array_key=function(a,myarr,key){
for (var arr2i = 0;arr2i < myarr.length; arr2i++) {
if(a[key]==myarr[arr2i][key]){
return true;
}
}
return false;
}
/*数组比较*/
var in_array=function(id,myarr,mode){
 mode = typeof mode !== 'undefined' ? mode : false;
for (var arr2i = 0;arr2i < myarr.length; arr2i++) {
if(id==myarr[arr2i]['id']){
 if (mode !== false) return myarr[arr2i][mode];
return true;
}
}
return false;
}
/*json数据处理*/
function jsonp(e){
	try {
	  return JSON.parse(e);
	}
	catch(err){
	  return [];
	}

}

/*判断是否是数组*/
var isArray=function (obj) {
return Object.prototype.toString.call(obj) === '[object Array]';
}
function formatLocation(longitude, latitude) {
	
	if (typeof longitude === 'string' && typeof latitude === 'string') {
		longitude = parseFloat(longitude)
		latitude = parseFloat(latitude)
	}
	return 		latitude.toFixed(6)+','+longitude.toFixed(6);
}
/*文章列表处理*/
var setdata=function(data, smode, content, pagtime,key){
key=key||'id';
var pxkey=''
 if(isArray(content) !=true)  content=[];
 if(isArray(data) !=true) return false
 var ajaxj = data.length;
 var k;
 for (var i = 0; i < ajaxj; i++) {

 k=i;
if('btime' in data[k]){
pxkey='btime'
  data[k]['timestr']=timeStamp2String(data[k]['btime'],true)
 }else{
pxkey='atime'
  data[k]['timestr']=timeStamp2String(data[k]['atime'],true)
 }
 if(!('key' in data[k])){ /*创建不重复的KEY */
 data[k]['key']=parseInt(Math.random()*100000000)+'-'+parseInt(Math.random()*100000000)
 }
  data[k][pxkey] = Number(data[k][pxkey]);
 if (pagtime > data[k][pxkey] || pagtime == 0) {pagtime = data[k][pxkey] }
  if('content' in data[k]) data[k]['content'] =data[k]['content'].replace(/\{m\}/g, imgcdn);
 if('img' in data[k] && data[k]['img']!=null){
 data[k]['img'] =data[k]['img'].replace(/\{m\}/g, '')
 data[k]['img'] = data[k]['img'].replace(imgswt, '')
 data[k]['imglist']=data[k]['img'].split(",");
 data[k]['img'] = data[k]['imglist'][0];
 data[k]['img'] = data[k]['img'].split("@")[0];
  }else{
  data[k]['imglist']=[];
 }

  if (in_array_key(data[k][key], content,key) == false) {
    if (smode == "up") {
    content.unshift(data[k])
    }else{
     content.push(data[k])
    }
   }
 }
 return [content, pagtime,ajaxj];
}




function add_fav(arr, myarr,del) {
    for (var arr2i = 0; arr2i < myarr.length; arr2i++) {
      if (arr['id'] == myarr[arr2i]['id']) {

        if(!del) myarr.push({
            id:arr['id'],
            content:arr['content'],
            mode:arr['mode'],
           });
        myarr.splice(arr2i, 1);
        return [myarr,false];
      }
    }
    if (myarr.length > 100) {
      myarr.splice(0, 10);
    }
    myarr.push({
        id:arr['id'],
        content:arr['content'],
        mode:arr['mode'],
     });
    return [myarr,true];
    }
 function add_array(arr, myarr,del) {
        for (var arr2i = 0; arr2i < myarr.length; arr2i++) {
          if (arr['id'] == myarr[arr2i]['id']) {
if(!del) myarr.push({id:arr['id'],name:arr['name'],url:arr['url']});
            myarr.splice(arr2i, 1);
            return [myarr,false];
          }
        }
if (myarr.length > 10) {myarr.splice(0, 4);}
myarr.push({id:arr['id'],name:arr['name'],url:arr['url']});
return [myarr,true];
      }
function Rad(d){
return d * Math.PI / 180.0;//经纬度转换成三角函数中度分表形式。
}
function GetDistance(lc1,lc2){
		if(typeof(lc1)!='string') return 'N';
		if(typeof(lc2)!='string') return 'N';	
		var lat1a=lc1.split(',');
		var lat2a=lc2.split(',');
return (Math.round((2 * Math.asin(Math.sqrt(Math.pow(Math.sin((Rad(lat1a[0]) - Rad(lat2a[0]))/2),2) +
Math.cos(Rad(lat1a[0]))*Math.cos(Rad(lat2a[0]))*Math.pow(Math.sin((Rad(lat1a[1]) - Rad(lat2a[1]))/2),2))))*6378.137 * 10000) / 10000).toFixed(1); //输出为公里 
}
/*基本函数：时间格式化，翻译，缩微图前缀，大图前缀，API请求前缀*/
module.exports ={
 setdata:setdata,/*加载数据格式化*/
 timestamp:parseInt(timestamp/100000),
 add_array:add_array,
 anz:'',
 sdkv:1,
 str_in_array:str_in_array,
 title2:'街拍第一站',/*短标题 */
 title:'街拍第一站 _ 全球街拍网,专业的街拍分享网站',
 keywords:'街拍第一站,街拍明星.街拍分享,街拍网站',
 description:'街拍第一站：全球街拍网，专业的街拍分享网站，提供各种类型的街拍图片',
 img:imgcdn+'c1c/c5e59060dd74c209cb35fa32a55dbf5c51f13826.jpg',
 www:'https://'+wzid+'.44api.com/',/*API请求前缀,就是您的域名*/
 localStorage:localStorage,
 isNumber:isNumber,
 votetype:['goods','nos','jubao','fav'],
 imgcdn:imgcdn,/*大图前缀*/
 imgswt:imgswt,
 lc:false,
 apiid:'lj44api_wzhc'+wzid,
 GetDistance:GetDistance,
 jsonp:jsonp,
 datalook:[],
 datafav:[],
 datalook2:[],
 datafav2:[],
 formatLocation:formatLocation,
 datalook3:[],
 datafav3:[],
 favurl:[],
 cloudCache:false,/*是否开启云加速，配置请查看https://smartprogram.baidu.com/docs/develop/api/net/request#%E4%BA%91%E5%8A%A0%E9%80%9F%E6%9C%8D%E5%8A%A1%E8%AF%B4%E6%98%8E */
 add_fav:add_fav,
 isArray:isArray,
 in_array:in_array,
timeStamp2String:timeStamp2String,
get_id:function(myarr,id){
 for (var arr2i = 0;arr2i < myarr.length; arr2i++) {
 if(id==myarr[arr2i]['id']){
 return myarr[arr2i];
 }
 }
 return false;
}
 }